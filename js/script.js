// Función para obtener la lista de razas de perros
async function fetchDogBreeds() {
    const response = await fetch('https://dog.ceo/api/breeds/list');
    const data = await response.json();
    return data.message;
}

// Función para cargar el combobox con razas de perros
async function cargarRazas() {
    const breeds = await fetchDogBreeds();
    const breedsSelect = document.getElementById('raza');
    breedsSelect.removeAttribute('disabled');

    breeds.forEach(breed => {
        const option = document.createElement('option');
        option.value = breed;
        option.text = breed;
        breedsSelect.add(option);
    });
}

// Función para obtener y mostrar la imagen de un perro de una raza seleccionada
async function fetchDogImage() {
    const selectedBreed = document.getElementById('raza').value;
    const imageUrl = `https://dog.ceo/api/breed/${selectedBreed}/images/random`;

    try {
        const response = await fetch(imageUrl);
        const data = await response.json();
        const dogImage = document.getElementById('dogImage');
        
        if (data.status === 'success') {
            dogImage.src = data.message;
            dogImage.alt = `Imagen de un perro ${selectedBreed}`;
        } else {
            console.error('Error al cargar la imagen del perro.');
        }
    } catch (error) {
        console.error('Error en la solicitud de la imagen del perro.', error);
    }
}

// Inicializar la lista de razas de perros al cargar la página
document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('btnCargar').addEventListener('click', cargarRazas);
    document.getElementById('btnFoto').addEventListener('click', fetchDogImage);
});
